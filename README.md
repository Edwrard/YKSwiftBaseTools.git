# YKSwiftBaseTools

[![CI Status](https://img.shields.io/travis/edward/YKSwiftBaseTools.svg?style=flat)](https://travis-ci.org/edward/YKSwiftBaseTools)
[![Version](https://img.shields.io/cocoapods/v/YKSwiftBaseTools.svg?style=flat)](https://cocoapods.org/pods/YKSwiftBaseTools)
[![License](https://img.shields.io/cocoapods/l/YKSwiftBaseTools.svg?style=flat)](https://cocoapods.org/pods/YKSwiftBaseTools)
[![Platform](https://img.shields.io/cocoapods/p/YKSwiftBaseTools.svg?style=flat)](https://cocoapods.org/pods/YKSwiftBaseTools)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

YKSwiftBaseTools is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'YKSwiftBaseTools'
```

## Author

edward, 534272374@qq.com

## License

YKSwiftBaseTools is available under the MIT license. See the LICENSE file for more info.
